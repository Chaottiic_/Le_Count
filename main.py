# -------------------------------------------------------------- #
# Information                                                    #
#                                                                #
#                                                                #
# Thank you for using my bot!                                    #
#                                                                #
# Please do not remove saying Chaottiic#0001 has made the bot    #
#                                                                #
# If you need support:                                           #
#                                                                #
# Discord - https://chaottiic.com/discord                        #
# My Discord - Chaottiic#0001                                    #
# Email - chaottiic@xenonode.com                                 #
#                                                                #
# Enjoy Counting!                                                #
#                                                                #
#                                                                #
# -------------------------------------------------------------- #


import discord
import json


# -------------------------------------------------------------- #
# Configuration                                                  #
#                                                                #
#                                                                #
# Token is the Discord Bot Token                                 #
#                                                                #
# CountingChannel is the Channel you want the Counting Game in.  #
#                                                                #
# Prefix is the Prefix for the Command                           #
#                                                                #
# SpamCheck is when the user can post multiple times in a row    #
#                                                                #
# DEBUG is to debug any issues.                                  #
#                                                                #
#                                                                #
# -------------------------------------------------------------- #


token = 'YOUR TOKEN' # Token ID

counting_channel = '374381596846850048' # Channel ID

prefix = '!' # Can be anything

spam_check = False # True

DEBUG = False # True


# -------------------------------------------------------------- #
#                                                                #
# Main Code                                                      #
#                                                                #
# -------------------------------------------------------------- #

client = discord.Client()

@client.event
async def on_message(message):

    if message.author == client.user:

        return

    with open('data.json', 'r') as jsonfile:

        data = jsonfile.read().replace('\n', '')

    value = json.loads(data)

    if message.channel.id == counting_channel:
        if message.content == value['next'] or message.content.startswith(value['next'] + ' '):

            if spam_check:

                if message.author.id == value['user']:

                    embed = discord.Embed(title="Error!", color=0xFF0000)
                    embed.add_field(name="SpamCheck is enabled!", value="You may not count after your own number!")
                    embed.set_footer(text=f"Executed by {message.author.name}#{message.author.discriminator}", icon_url=message.author.avatar_url)
                    await client.send_message(message.author, embed=embed)

                    print(f"[Success]  Message Deleted! ({message.author.name}) [{message.author.id}]")
                    await client.delete_message(message)

                    return

            next_number = str(int(value['next']) + 1)
            value['next'] = next_number
            current_number = str(int(value['current']) + 1)
            value['current'] = current_number
            value['user'] = message.author.id

            with open("data.json", "w") as newdata:

                json.dump(value, newdata)
                print(f"[Success] Updated! ({message.author.name}) [{message.author.id}] - {value['next']}")

        else:

            if DEBUG:

                print(f'[DEBUG] Message Deleted! ({message.author.name}) [{message.author.id}]')
                return

            else:

                embed = discord.Embed(title="Error!", color=0xFF0000)
                embed.add_field(name="You've entered the wrong number!", value=f"Please enter {value['next']}", inline=False)
                embed.set_footer(text=f"Executed by {message.author.name}#{message.author.discriminator}", icon_url=message.author.avatar_url)
                await client.send_message(message.author, embed=embed)

                print(f'[Success] Message Deleted! ({message.author.name}) [{message.author.id}] | {message.content}')
                await client.delete_message(message)
    else:

        if message.content.startswith(prefix + 'count'):

            embed = discord.Embed(title="Counting Game Info", color=0x15A1EE)
            embed.add_field(name="Current Number", value=value['current'], inline=True)
            embed.add_field(name="Next Number", value=value['next'], inline=True)
            embed.add_field(name="Last Msg User", value=f"<@{value['user']}>", inline=True)
            embed.add_field(name="Counting Game Channel", value=f"<#{counting_channel}>", inline=True)
            embed.set_footer(text=f"Executed by {message.author.name}#{message.author.discriminator}", icon_url=message.author.avatar_url)
            await client.send_message(message.channel, embed=embed)
            print(f'[Success] Command Executed! ({message.author.name}) [{message.author.id}] - {prefix} count')

        if message.content.startswith(prefix + 'about'):

            embed = discord.Embed(title="About " + client.user.name, description="Hi! I'm {client.user.name}.", color=0x15A1EE)
            embed.add_field(name="My Creator", value="Chaottiic#0001 has created me with love :heart:", inline=True)
            embed.add_field(name="My ID", value=client.user.id, inline=False)
            embed.add_field(name="About me", value="I'm an advanced (not lol) counting bot that will watch you count!", inline=False)
            embed.set_footer(text=f"Executed by {message.author.name}#{message.author.discriminator}", icon_url=message.author.avatar_url)
            await client.send_message(message.channel, embed=embed)
            print(f'[Success] Command Executed! ({message.author.name}) [{message.author.id}] - {prefix} about')

        if DEBUG:

            if message.content.startswith(prefix + 'debug'):
                embed = discord.Embed(color=0x15A1EE)
                embed.add_field(name="ID", value=client.user.id, inline=False)
                embed.add_field(name="Current Number", value=value['current'], inline=True)
                embed.add_field(name="Next Number", value=value['next'], inline=True)
                embed.add_field(name="Last Msg User", value=f"<@{value['user']}>", inline=True)
                embed.add_field(name="Counting Game Channel", value=f"<#{counting_channel}>", inline=True)
                embed.add_field(name="DEBUG", value=DEBUG, inline=True)
                embed.add_field(name="Prefix", value=prefix, inline=True)
                embed.add_field(name="Current Channel ID", value=message.channel.id, inline=True)
                embed.set_footer(text=f"Executed by {message.author.name}#{message.author.discriminator}", icon_url=message.author.avatar_url)
                await client.send_message(message.channel, embed=embed)
                print(f'[DEBUG] Command Executed! ({message.author.name}) [{message.author.id}] - {prefix} DEBUG')



@client.event
async def on_ready():
    print(f'You are currently starting {client.user.name} ({client.user.id})')
    print(' ')
    print(' ')
    print(' ')
    print(' ')
    print(' ')
    print(' ')
    print('//--------------------------------------------------------------------//')
    print('//                                                                    //')
    print('//       Created by Chaottiic (Chaottiic#0001)                        //')
    print('//                                                                    //')
    print('//                                                                    //')
    print('//       Website - https://chaottiic.com                              //')
    print('//                                                                    //')
    print('//       Discord - https://chaottiic.com/discord                      //')
    print('//                                                                    //')
    print('//                                                                    //')
    print('//--------------------------------------------------------------------//')

client.run(token)